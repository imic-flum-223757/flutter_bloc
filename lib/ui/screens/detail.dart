import 'package:flutter/material.dart';
import 'package:flutter_bloc/models/product.dart';

class DetailScreen extends StatelessWidget {
  final Product product;

  const DetailScreen({Key key, this.product}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('${product.name}'),),
    );
  }
}
