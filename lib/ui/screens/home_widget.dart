import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/models/product.dart';
import 'package:flutter_bloc/product_data.dart';
import 'package:flutter_bloc/ui/screens/cart_screen.dart';
import 'package:flutter_bloc/ui/widgets/product_item_clip.dart';
import 'package:flutter_bloc/view_model/cart_vm.dart';
import 'package:provider/provider.dart';

class HomeWidget {
  Widget buildHeaderText(BuildContext context) {
    var cartModel = Provider.of<CartViewModel>(context);
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text('MyBag'),
        InkWell(
          child: Text(
            'Giỏ hàng ${cartModel.products.length}',
            style: TextStyle(fontSize: 30),
          ),
          onTap: ()=>Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => CartScreen()),
          ))
      ],
    );
  }

  Widget buildListItems(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(10),
      shrinkWrap: true,
      children: List.generate(
        products.length,
        (index) => ProductItemClip(product: Product.fromJson(products[index])),
      ),
    );
  }
}
