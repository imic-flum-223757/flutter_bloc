import 'package:flutter/material.dart';
import 'package:flutter_bloc/base/bloc_provider.dart';
import 'package:flutter_bloc/blocs/login_bloc.dart';
import 'package:flutter_bloc/ui/widgets/custom_text_field.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Login Rxdart'),
      ),
      body: BlocProvider<LoginBloc>(
        bloc: LoginBloc(),
        builder: (_, bloc, __) {
          return SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.all(10),
              child: Column(
                children: [
                  CustomTextField(
                    changeText: bloc.setEmail,
                    textStream: bloc.emailOnchange,
                    placeholder: 'Enter Email',
                  ),
                  CustomTextField(
                    changeText: bloc.setPassword,
                    textStream: bloc.passwordOnchange,
                    security: true,
                    placeholder: 'Enter Password',
                  ),
                  StreamBuilder<bool>(
                      initialData: false,
                      stream: bloc.btnOnChange,
                      builder: (_, snapshot) {
                        return RaisedButton(
                            child: Text('Login'),
                            onPressed:
                                snapshot.data ? () => bloc.login() : null);
                      })
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
