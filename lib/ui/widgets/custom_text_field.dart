import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  final Stream textStream;
  final bool security;
  final Function changeText;
  final String placeholder;

  const CustomTextField(
      {Key key,
      @required this.textStream,
      this.security = false,
      @required this.changeText, this.placeholder})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<String>(
      stream: textStream,
      builder: (_, snapshot) {
        return TextField(
          onChanged: changeText,
          obscureText: security,
          decoration: InputDecoration(
            hintText: placeholder,
              errorText: snapshot.hasError ? snapshot.error : ''),
        );
      },
    );
  }
}
