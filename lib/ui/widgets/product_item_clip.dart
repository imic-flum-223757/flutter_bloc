import 'package:flutter/material.dart';
import 'package:flutter_bloc/models/product.dart';
import 'package:flutter_bloc/ui/screens/detail.dart';
import 'package:flutter_bloc/view_model/cart_vm.dart';
import 'package:provider/provider.dart';

class ProductItemClip extends StatelessWidget {
  final Product product;

  const ProductItemClip({Key key, this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => DetailScreen(product: this.product))),
      child: Container(
        margin: EdgeInsets.only(top: 10),
        child: IntrinsicHeight(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: 100,
                height: 70,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    border: Border.all(width: 1, color: Color(0xFFB74093))),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(20),
                  child: Image.asset('assets/images/${product.img}',
                      fit: BoxFit.cover),
                ),
              ),
              Expanded(
                child: Container(
                  color: Colors.red,
                  padding: EdgeInsets.only(left: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        '${product.name}',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      Consumer<CartViewModel>(builder: (_, model, child) {
                        return RaisedButton(
                            onPressed: () => model.addCart(product), child: Icon(Icons.add),);
                      }),
                      SizedBox(height: 10),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [Text('${product.price}'), Text('x1')],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
