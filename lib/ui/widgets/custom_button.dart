import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  final Widget child;
  final Function onPress;

  const CustomButton({Key key, @required this.child, @required this.onPress})
      : super(key: key);

  factory CustomButton.icon(
      {@required IconData icon, @required Function onPress}) = _CustomButtonIcon;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPress,
      child: Container(
        child: child,
      ),
    );
  }
}

class _CustomButtonIcon extends CustomButton {
  final IconData icon;
  final Function onPress;

  _CustomButtonIcon({Key key, this.icon, this.onPress});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPress,
      child: Container(
        child: Row(
          children: [Icon(icon), Text('Text Icon')],
        ),
      ),
    );
  }
}
