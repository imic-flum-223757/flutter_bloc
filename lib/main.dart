import 'package:flutter/material.dart';
import 'package:flutter_bloc/ui/screens/home_news_rx.dart';
import 'package:flutter_bloc/view_model/cart_vm.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider<CartViewModel>(create: (_) => CartViewModel())
        ],
        child: MaterialApp(
          title: 'Flutter Demo',
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            primarySwatch: Colors.blue,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          home: HomeNewsRx(),
        ));
  }
}
