import 'package:flutter_bloc/base/bloc_provider.dart';
import 'package:flutter_bloc/data/repository/news_repository.dart';
import 'package:rxdart/rxdart.dart';

class NewsBloc extends BaseBloc {
  final _news = BehaviorSubject<List<dynamic>>.seeded([]);
  final _newsRepository = NewsRepository();

  /// Stream
  Stream<List<dynamic>> get newsOnchange => _news.stream;

  // setNews()

  void onLoading() async {
    setLoading(true);
    await Future.delayed(Duration(seconds: 3));
    _news.sink.add(await _newsRepository.getNews(1));
    setLoading(false);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _news.close();
    super.dispose();
  }
}
